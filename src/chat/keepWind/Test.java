package chat.keepWind;

import java.io.File;

import chat.keepWind.bean.DataPackage;
import chat.keepWind.event.ReceiveFile;
import chat.keepWind.util.GlobalConst;
import io.netty.util.CharsetUtil;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DataPackage dataPkg = new DataPackage();
		
		long start = System.currentTimeMillis();
		dataPkg.setFromIp("34.1.172.149");
		dataPkg.setToIp("34.1.172.149");
		dataPkg.setMsgType(GlobalConst.TYPE_SND_FILE);
		dataPkg.setMsgContent("".getBytes(CharsetUtil.UTF_8));
		dataPkg.setSrcPath("d:\\test.jar");
		dataPkg.setFileName("test.jar");
		dataPkg.setDistPath("e:\\test.jar");
		dataPkg.setFileLen(new File("d:\\test.jar").length());
		ReceiveFile rf = new ReceiveFile();
		rf.receive();
		long end = System.currentTimeMillis();
		System.out.println((end-start)/1000);
	}

}
