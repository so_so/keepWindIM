package chat.keepWind.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.StringTokenizer;

public class NetUtil {
	/**
	 * 获取本地IP地址
	 * @return
	 * @throws Exception
	 */
	public static String getLocalIp() {
		String ip="";
	    try {
	        InetAddress candidateAddress = null;
	        // 遍历所有的网络接口
	        for (Enumeration<NetworkInterface> ifaces = NetworkInterface.getNetworkInterfaces(); 
	        		ifaces.hasMoreElements(); ) {
	            NetworkInterface iface =  ifaces.nextElement();
	            // 在所有的接口下再遍历IP
	            for (Enumeration<InetAddress> inetAddrs = iface.getInetAddresses(); 
	            		inetAddrs.hasMoreElements(); ) {
	                InetAddress inetAddr =  inetAddrs.nextElement();
	                if (!inetAddr.isLoopbackAddress()) {// 排除loopback类型地址
	                    if (inetAddr.isSiteLocalAddress()) {
	                        // 如果是site-local地址，就是它了
	                    	ip = inetAddr.getHostAddress();
	                    	if(ip.split("\\.").length==4){
	                    		return ip;
	                    	}
	                        
	                    } else if (candidateAddress == null) {
	                        // site-local类型的地址未被发现，先记录候选地址
	                        candidateAddress = inetAddr;
	                    }
	                }
	            }
	        }
	        if (candidateAddress != null) {
	        	ip = candidateAddress.getHostAddress();
	        	if(ip.split("\\.").length==4){
            		return ip;
            	}
	        }
	        // 如果没有发现 non-loopback地址.只能用最次选的方案
	        ip = InetAddress.getLocalHost().getHostAddress();
	        return ip;
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    return "";
	}
	/**
	 * 获取 本机 局域网内所有在线IP地址
	 * @return
	 */
	public static List<String> getLanIPs(){
		 List<String> list = new ArrayList<>();
	        try {  
	            
	            Process process = Runtime.getRuntime().exec("arp -a");  
	            InputStreamReader inputStr = new InputStreamReader(  
	                    process.getInputStream(), "GBK");  
	            BufferedReader br = new BufferedReader(inputStr);  
	            String temp = "";  
	            br.readLine();  
	            br.readLine();  
	            br.readLine();
	            //正文
	            while ((temp = br.readLine()) != null) {  
	                if (temp!=null && !"".equals(temp)
	                		&& temp.indexOf("动态")>0) {  
	                    StringTokenizer tokens = new StringTokenizer(temp);  
	                  
	                   list.add(tokens.nextToken());
	                }  
	            }  
	            process.destroy();  
	            br.close();  
	            inputStr.close();  
	        } catch (Exception e) {  
	            e.printStackTrace();  
	        } 
	    return list;
	}
	/**
	 * 检测IP地址是否合法
	 * @param ip
	 * @return
	 */
	public static boolean checkIp(String ip) {
		String[] arr = ip.split("\\.");
		if(arr.length!=4){
			return false;
		}
		int len=ip.length();
		int x=0;
		for(int j=0;j<len;j++) {
			if((ip.charAt(j)>47&&ip.charAt(j)<59)||ip.charAt(j)=='.'){
				x++;
			}
		}
		if(x!=len){
			return false;
		}
		
		int a=Integer.valueOf(arr[0]);
		int b=Integer.parseInt(arr[1]);
		int c=Integer.parseInt(arr[2]);
		int d=Integer.parseInt(arr[3]);
		if(a>=0 && a<=255 && b>=0 && b<=255&& c>=0 && c<=255&&d>=0 && d<=255  ){
			return true;
		}else {
			return false;
		}
	} 
}
