package chat.keepWind.util;

import java.io.File;

public class GlobalConst {
	
	
	//发送方默认端口
	public final static int SND_PORT = 9171;
	//接收方端口
	public final static int RCV_PORT = 9172;
	//文件端口
	public final static int FILE_PORT = 9173;
	//文件一次传送的字节数
	public final static int BYTE_NUM = 1024*60;
	//系统临时目录路径
	public final static String TMP_ROOT_PATH=System.getProperty("java.io.tmpdir");
	//屏幕截图路径
	public final static String CAP_IMG_PATH=TMP_ROOT_PATH+"keepWindIM"+File.separator
			+"screenImg"+File.separator;
	/**
	 * 消息类型
	 */
	//上线
	public final static String TYPE_LOGIN="0";
	//上线回应
	public final static String TYPE_LOGIN_REP="1";
	//下线
	public final static String TYPE_LOGOUT="2";
	//普通文本
	public final static String TYPE_TXT="3";
	//接受到文件类型消息
	public final static String TYPE_FILE="4";
	//发送文件
	public final static String TYPE_SND_FILE="5";
	//准备接受文件
	public final static String TYPE_RCV_FILE="6";
	//接受截图
	public final static String TYPE_RCV_CAPIMG="7";
	//文件传输完成
	public final static String TYPE_FILE_COMPLETE="8";
}
