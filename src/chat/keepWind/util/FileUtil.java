package chat.keepWind.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import chat.keepWind.bean.Config;

public class FileUtil {
	public static Config readConfig() throws IOException, 
		ClassNotFoundException{
		Config config = null;
		File file = new File(GlobalConst.TMP_ROOT_PATH+"keepWindIM"+File.separator+"config.ser");
		try(InputStream in = new FileInputStream(file);
				ObjectInputStream objIn = new ObjectInputStream(in)){
			config = (Config)objIn.readObject();
		} 
		return config;
	}
	
	public static void writeConfig(Config config) throws IOException{
		File file = new File(GlobalConst.TMP_ROOT_PATH+"keepWindIM"+File.separator+"config.ser");
		try(OutputStream out = new FileOutputStream(file);
				ObjectOutputStream objOut = new ObjectOutputStream(out)){
			objOut.writeObject(config);
		}
	}
	
}
