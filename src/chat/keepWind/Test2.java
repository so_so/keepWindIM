package chat.keepWind;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import chat.keepWind.bean.DataPackage;
import chat.keepWind.event.SendCapImg;
import chat.keepWind.util.GlobalConst;

public class Test2 {

	public static void main(String[] args) {
		List<DataPackage> imgs = new ArrayList<>();
		DataPackage dataPkg = new DataPackage();
		
		long start = System.currentTimeMillis();
		dataPkg.setFromIp("34.1.172.149");
		dataPkg.setToIp("34.1.172.149");
		dataPkg.setMsgType(GlobalConst.TYPE_RCV_CAPIMG);
		dataPkg.setSrcPath("d:\\test.png");
		dataPkg.setFileName("test.png");
		dataPkg.setDistPath("e:\\test.png");
		dataPkg.setFileLen(new File("d:\\test.png").length());
		imgs.add(dataPkg);
		
		
		DataPackage dataPkg2 = new DataPackage();
		dataPkg2.setFromIp("34.1.172.149");
		dataPkg2.setToIp("34.1.172.149");
		dataPkg2.setMsgType(GlobalConst.TYPE_RCV_CAPIMG);
		dataPkg2.setSrcPath("d:\\test1.png");
		dataPkg2.setFileName("test1.png");
		dataPkg2.setDistPath("e:\\test1.png");
		dataPkg2.setFileLen(new File("d:\\test1.png").length());
		imgs.add(dataPkg2);
		
		SendCapImg snd = new SendCapImg();
		snd.send("34.1.172.149", imgs);
		
		long end = System.currentTimeMillis();
		System.out.println((end-start)/1000);
	}

}
