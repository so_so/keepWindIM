package chat.keepWind.bean;

import java.util.Arrays;

import chat.keepWind.util.GlobalConst;
import io.netty.util.CharsetUtil;

public class DataPackage {
	private byte flag = 1; //标识 1：主动发送（默认）2：接收方反馈
	private byte[] msgContent="".getBytes(CharsetUtil.UTF_8);//消息内容
	private String msgType;//消息类型
	private String fromIp;//发送方IP
	private int port=GlobalConst.SND_PORT;//发送方端口；发送上线通知时，端口随机，防止被占用
	private String toIp; // 接收方ip
	private String nickName=""; //发送方昵称
	private String sendTime=""; //发送时间
	
	private String fileName="";//传送文件名
	private String srcPath=""; //文件原地址
	private String distPath="";//文件目标地址
	private long point=0 ; //文件的位置指针
	private long fileLen=0; //文件总字节数
	private int fileNo=0; //文件序号
	
	public byte[] getMsgContent() {
		return msgContent;
	}
	public void setMsgContent(byte[] msgContent) {
		this.msgContent = msgContent;
	}
	public String getMsgType() {
		return msgType;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	public String getFromIp() {
		return fromIp;
	}
	public void setFromIp(String fromIp) {
		this.fromIp = fromIp;
	}
	
	public String getToIp() {
		return toIp;
	}
	public void setToIp(String toIp) {
		this.toIp = toIp;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getSendTime() {
		return sendTime;
	}
	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}
	
	public String getSrcPath() {
		return srcPath;
	}
	public void setSrcPath(String srcPath) {
		this.srcPath = srcPath;
	}
	public String getDistPath() {
		return distPath;
	}
	public void setDistPath(String distPath) {
		this.distPath = distPath;
	}
	public long getPoint() {
		return point;
	}
	public void setPoint(long point) {
		this.point = point;
	}
	
	public long getFileLen() {
		return fileLen;
	}
	public void setFileLen(long fileLen) {
		this.fileLen = fileLen;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	
	public byte getFlag() {
		return flag;
	}
	public void setFlag(byte flag) {
		this.flag = flag;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	
	public int getFileNo() {
		return fileNo;
	}
	public void setFileNo(int fileNo) {
		this.fileNo = fileNo;
	}
	@Override
	public String toString() {
		return "DataPackage [flag=" + flag + ", msgContent=" + Arrays.toString(msgContent) + ", msgType=" + msgType
				+ ", fromIp=" + fromIp + ", port=" + port + ", toIp=" + toIp + ", nickName=" + nickName + ", sendTime="
				+ sendTime + ", fileName=" + fileName + ", srcPath=" + srcPath + ", distPath=" + distPath + ", point="
				+ point + ", fileLen=" + fileLen + ", fileNo=" + fileNo + "]";
	}
	
}
