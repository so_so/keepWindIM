package chat.keepWind.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Config implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Map<String,User> userMap = new HashMap<>();
	
	private String localNickName;
	private List<String>  ips = new ArrayList<>();
	
	
	public String getLocalNickName() {
		return localNickName;
	}
	public void setLocalNickName(String localNickName) {
		this.localNickName = localNickName;
	}
	public List<String> getIps() {
		return ips;
	}
	public void setIps(List<String> ips) {
		this.ips = ips;
	}
	public Map<String, User> getUserMap() {
		return userMap;
	}
	public void setUserMap(Map<String, User> userMap) {
		this.userMap = userMap;
	} 
	
}
