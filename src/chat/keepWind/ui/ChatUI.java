
package chat.keepWind.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.SystemColor;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;

import org.jb2011.lnf.beautyeye.ch3_button.BEButtonUI;

/**
 * 
 * @author
 */
public class ChatUI  {
	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1453456465L;
	JDesktopPane desktop = new JDesktopPane();
	
	/**
	 * 文本域：存放聊天记录
	 */
	private  JTextPane dsiplChatArea = null;

	/**
	 * 文本域：聊天输入
	 */
	private  JTextPane inputChatArea = null;

	/**
	 * 发送截图标记false为非截图发送
	 */
	private  boolean isCapScr = false;
	/**
	 * 文件按钮
	 */
	private JButton fileBtn = null;
	/**
	 * 截图按钮
	 */
	private JButton capBtn = null;
	/**
	 * 表情按钮
	 */
	private JButton lookBtn = null;
	/**
	 * 清屏按钮
	 */
	private JButton clearBtn = null;
	/**
	 * 关闭按钮
	 */
	private JButton closeBtn = null;

	/**
	 * 发送按钮
	 */
	private JButton sendBtn = null;
	/**
	 * 窗体
	 */
	private JFrame win = null;// 窗体

	/**
	 * 添加按钮等
	 */
	JPanel winMainJP = null;

	/**
	 * 聊天发送者号码
	 */
	private  String sourceId;// 聊天发送者

	/**
	 * 聊天接收者号码
	 */
	private  String distId;// 聊天目标
	
	private String distNickName;// 聊天目标的昵称
	/**
	 * 显示消息文本区
	 */
	JScrollPane displayMessjSP = null;

	/**
	 * 发送消息文本区
	 */
	JScrollPane sendMessjSP = null;


	// 菜单栏
	private JMenuBar mb = new JMenuBar();
	
	private JMenu setMenu = new JMenu("设置");
	private JMenuItem sendItem = new JMenuItem("发送");
	

	// 窗体高度、宽度
	private int width = 525;
	private int height = 600;

	/**
	 * 构造函数
	 */
	public ChatUI(String sourceId, String distId,String distNickName) {
		this.sourceId = sourceId;
		this.distId = distId;
		this.distNickName = distNickName;
	}

	/**
	 * 此方法初始化
	 */
	public JFrame getwinFrame() {
		if (win == null) {
			
			win = new JFrame();
			//win.setSize(width, height);
			win.setContentPane(getwinMainJP());
			//setMenu.add(sendItem);
		
			mb.add(setMenu);
			
			win.setJMenuBar(mb);
			//Toolkit toolkit = win.getToolkit();
			win.setIconImage(Toolkit.getDefaultToolkit().getImage(
					getClass().getResource("/picture/chat.png")));
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			// 计算屏幕中心坐标
			int centerX = screenSize.width / 2;
			int centerY = screenSize.height / 2;
			// 设置窗体居中
			win.setBounds(centerX - width / 2, centerY - height / 2 - 30,
					width, height);
			win.setVisible(true);
			win.setResizable(false);
			win.getRootPane().setDefaultButton(sendBtn);
		}
		return win;
	}

	/**
	 * 此方法初始化winMainJP
	 */
	public JPanel getwinMainJP() {
		if (winMainJP == null) {
			winMainJP = new JPanel();
			winMainJP.setBackground(Color.white);
			winMainJP.setLayout(null);
		}
		if (dsiplChatArea == null) {
			dsiplChatArea = new JTextPane();
		}
		if (inputChatArea == null) {
			inputChatArea = new JTextPane();
			inputChatArea.setFocusCycleRoot(true);
		}
		if (closeBtn == null) {
			closeBtn = new JButton();
			closeBtn.setFocusPainted(true);
			closeBtn.setFont(new Font("黑体", Font.PLAIN, 14));
		}
		if (sendBtn == null) {
			sendBtn = new JButton();
			sendBtn.setFocusPainted(true);
		}
		if (displayMessjSP == null) {
			displayMessjSP = new JScrollPane();
		}
		if (sendMessjSP == null) {
			sendMessjSP = new JScrollPane();
		}
		if (setMenu == null) {
			setMenu = new JMenu("设置");
		}
		if (sendItem == null) {
			sendItem = new JMenuItem("发送");
			sendItem.setFocusPainted(true);
		}
	
		
		if (lookBtn == null) {
			lookBtn = new JButton();
		}
		if (fileBtn == null) {
			fileBtn = new JButton();
		}
		if (capBtn == null) {
			capBtn = new JButton();
		}
		if (clearBtn == null) {
			clearBtn = new JButton();
		}
		dsiplChatArea.setBackground(Color.white);
		dsiplChatArea.setFont(new Font("Dialog", Font.PLAIN, 12));
		dsiplChatArea.setForeground(Color.black);
		dsiplChatArea.setBounds(6, 9, 100, 200);
		dsiplChatArea.setEditable(false);
		displayMessjSP.setBounds(new Rectangle(6, 9, 460, 280));
		displayMessjSP
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		displayMessjSP.getViewport().add(dsiplChatArea);
		
		
		
		inputChatArea.setFont(new Font("黑体", Font.PLAIN, 14));
		inputChatArea.setBounds(new Rectangle(6, 207, 200, 60));
		sendMessjSP.setBounds(6, 307, 460, 135);
		sendMessjSP
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		sendMessjSP.getViewport().add(inputChatArea);
		closeBtn.setBounds(new Rectangle(45, 453, 80, 24));
		closeBtn.setForeground(SystemColor.menuText);
		closeBtn.setText("关闭");
		closeBtn.setUI(new BEButtonUI()
				.setNormalColor(BEButtonUI.NormalColor.red));
		closeBtn.setFont(new Font("黑体", Font.PLAIN, 12));
		sendBtn.setBounds(new Rectangle(350, 453, 80, 24));
		sendBtn.setFont(new Font("黑体", Font.PLAIN, 12));
		sendBtn.setForeground(SystemColor.menuText);
		sendBtn.setText("发送");
		sendBtn.setUI(new BEButtonUI()
				.setNormalColor(BEButtonUI.NormalColor.green));
		
		lookBtn.setBounds(new Rectangle(20, 285, 22, 22));
		lookBtn.setIcon(new ImageIcon(Toolkit
				.getDefaultToolkit().getImage(getClass().getResource("/picture/lookBtn.png"))));
		lookBtn.setBackground(null);
		lookBtn.setToolTipText("表情");
		
		fileBtn.setBounds(new Rectangle(44, 285, 22, 22));
		fileBtn.setIcon(new ImageIcon(Toolkit
				.getDefaultToolkit().getImage(getClass().getResource("/picture/fileBtn.png"))));
		fileBtn.setToolTipText("文件");
		capBtn.setBounds(new Rectangle(68, 285, 22, 22));
		capBtn.setIcon(new ImageIcon(Toolkit
				.getDefaultToolkit().getImage(getClass().getResource("/picture/capBtn.png"))));
		capBtn.setToolTipText("截图");
		clearBtn.setBounds(new Rectangle(92, 285, 22, 22));
		clearBtn.setIcon(new ImageIcon(Toolkit
				.getDefaultToolkit().getImage(getClass().getResource("/picture/clearBtn.png"))));
		clearBtn.setToolTipText("清屏");
		
		winMainJP.setEnabled(true);
		winMainJP.add(closeBtn);
		winMainJP.add(sendBtn);
		winMainJP.add(lookBtn);
		winMainJP.add(fileBtn);
		winMainJP.add(capBtn);
		winMainJP.add(clearBtn);
		winMainJP.add(displayMessjSP);
		winMainJP.add(sendMessjSP);
		return winMainJP;
	}

	public JFrame getWin() {
		return win;
	}

	public void setWin(JFrame win) {
		this.win = win;
	}

	public JTextPane getInputChatArea() {
		return inputChatArea;
	}

	public void setInputChatArea(JTextPane inputChatArea) {
		this.inputChatArea = inputChatArea;
	}

	public boolean isCapScr() {
		return isCapScr;
	}

	public void setCapScr(boolean isCapScr) {
		this.isCapScr = isCapScr;
	}

	public JButton getSendBtn() {
		return sendBtn;
	}

	public void setSendBtn(JButton sendBtn) {
		this.sendBtn = sendBtn;
	}

	public JTextPane getDsiplChatArea() {
		return dsiplChatArea;
	}

	public void setDsiplChatArea(JTextPane dsiplChatArea) {
		this.dsiplChatArea = dsiplChatArea;
	}

	public JButton getCloseBtn() {
		return closeBtn;
	}

	public void setCloseBtn(JButton closeBtn) {
		this.closeBtn = closeBtn;
	}

	public JMenuItem getSendItem() {
		return sendItem;
	}

	public void setSendItem(JMenuItem sendItem) {
		this.sendItem = sendItem;
	}

	public JButton getLookBtn() {
		return lookBtn;
	}

	public void setLookBtn(JButton lookBtn) {
		this.lookBtn = lookBtn;
	}

	
	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getDistId() {
		return distId;
	}

	public void setDistId(String distId) {
		this.distId = distId;
	}

	public String getDistNickName() {
		return distNickName;
	}

	public void setDistNickName(String distNickName) {
		this.distNickName = distNickName;
	}

	public JButton getFileBtn() {
		return fileBtn;
	}

	public void setFileBtn(JButton fileBtn) {
		this.fileBtn = fileBtn;
	}

	public JButton getCapBtn() {
		return capBtn;
	}

	public void setCapBtn(JButton capBtn) {
		this.capBtn = capBtn;
	}

	public JButton getClearBtn() {
		return clearBtn;
	}

	public void setClearBtn(JButton clearBtn) {
		this.clearBtn = clearBtn;
	}
	
	
}
