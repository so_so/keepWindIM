/**
 * 好友列表显示
 */

package chat.keepWind.ui;

import java.awt.Component;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

public class JTreeCellRenderer extends DefaultTreeCellRenderer {


	private static final long serialVersionUID = 1L;

	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value,
			boolean sel, boolean expanded, boolean leaf, int row,
			boolean hasFocus) {

		super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf,
				row, hasFocus);
		
		
		
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
		if (node.getUserObject() instanceof JTreeBean) {
		} else {
			System.out.println(node.getUserObject() + "admin");
		}
		JTreeBean jtb = (JTreeBean) node.getUserObject();
		setIcon(jtb.getIcon());
		setText(jtb.getNick());
		setFont(jtb.getFont());
		setTextNonSelectionColor(jtb.getColor());
		//setBackground(null);
		return this;
	}
	
}
