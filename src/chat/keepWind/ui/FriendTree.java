
package chat.keepWind.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.util.Enumeration;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import chat.keepWind.bean.User;


/**
 * 
 * @author
 */
public class FriendTree {
	
	
	private JTreeBean root_bean = new JTreeBean("Group", new ImageIcon(Toolkit
			.getDefaultToolkit().getImage(
					getClass().getResource("/picture/wind_s.png"))),Color.BLACK,"Group");
	
	private JTreeBean online_bean = new JTreeBean("在线用户", new ImageIcon(Toolkit
			.getDefaultToolkit().getImage(
					getClass().getResource("/picture/chat_s.png"))),Color.BLACK,"在线用户");
	/**
	 * 树根节点
	 */
	private DefaultMutableTreeNode root;
	/**
	 * 在线用户
	 */
	private DefaultMutableTreeNode online;
	
	private DefaultTreeModel treemode;
	/**
	 * 构造一棵树
	 */
	private JTree tree;

	
	/**
	 * 树初始化
	 */
	public JTree initTree(){
		root_bean.setFont(new Font("宋体", Font.PLAIN, 16));
		root = new DefaultMutableTreeNode(root_bean);
		treemode = new DefaultTreeModel(root);
		tree = new JTree(treemode);
		tree.setBackground(Color.white);
		
		online_bean.setFont(new Font("宋体", Font.PLAIN, 16));
		online = new DefaultMutableTreeNode(online_bean);
		
		treemode.insertNodeInto(online, root, 0);
	
		tree.setCellRenderer(new JTreeCellRenderer());//设置将要绘制单元格
		
		tree.scrollPathToVisible(new TreePath(online.getPath()));//展开
		
		
		return tree;
	}

	/**
	 * 根据id在“在线的好友"的子节点中添加该以id为节点的子节点
	 */
	public void addOnlineUser(String ip,String nick) {
		//treemode = (DefaultTreeModel) tree.getModel();// 得到DefaultTreeModel
		
	
		ImageIcon icon = new ImageIcon(Toolkit.getDefaultToolkit().getImage(
						getClass().getResource("/picture/dfthead_s.png")));
		//icon.setImage(icon.getImage().getScaledInstance(25, 25, Image.SCALE_DEFAULT));
		
		JTreeBean online_ico = new JTreeBean(nick, icon,Color.BLACK,ip);
	
		DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(online_ico);
		
		treemode.insertNodeInto(newNode, online, online.getChildCount());
		tree.getSelectionModel().setSelectionMode(
				TreeSelectionModel.SINGLE_TREE_SELECTION);// 只能选取一个节点
		tree.scrollPathToVisible(new TreePath(newNode.getPath()));
	}
	/**
	 * 添加所有的在线 用户
	 * @param list
	 */
	public void addAllUser(List<User> list) {
		//treemode = (DefaultTreeModel) tree.getModel();// 得到DefaultTreeModel
		
		int len = list.size();
		for(int i=0;i<len;i++){
			User user = list.get(i);
			String ip = user.getIp();
			String nick = user.getNickName();
			
			ImageIcon icon = new ImageIcon(Toolkit.getDefaultToolkit().getImage(
					getClass().getResource("/picture/dfthead_s.png")));
			//icon.setImage(icon.getImage().getScaledInstance(16, 16, Image.SCALE_DEFAULT));
			
			JTreeBean online_ico = new JTreeBean(nick, icon,Color.BLACK,ip);
		
			DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(online_ico);
			
			treemode.insertNodeInto(newNode, online, online.getChildCount());
			tree.getSelectionModel().setSelectionMode(
					TreeSelectionModel.SINGLE_TREE_SELECTION);// 只能选取一个节点
			
			if(i==len-1){
				tree.scrollPathToVisible(new TreePath(newNode.getPath()));//展开
			}

		}
		
	}
	/**
	 * 移除不在线 用户
	 */
	@SuppressWarnings({ "unchecked" })
	public void delOfflineUser(String ip) {
		DefaultMutableTreeNode n = null;
		for (Enumeration<DefaultMutableTreeNode> e = online.children(); 
				e.hasMoreElements();) {
			n =  e.nextElement();
			JTreeBean JTB_r = (JTreeBean) n.getUserObject();
			if (JTB_r.getIp().equals(ip)) {
				online.remove(n);// 查找在线的列表中有没有此ID有则删除
				treemode.reload();
				tree.scrollPathToVisible(new TreePath(n.getPath()));
			}
		}
		
	}
	/**
	 * 删除 所有用户
	 */
	public void delAllUser(){
		online.removeAllChildren();
		treemode.reload();
	}
	
	

	public JTree getTree() {
		return tree;
	}

	public void setTree(JTree tree) {
		this.tree = tree;
	}
	
}
