/**
 * 客户端主操作界面
 */

package chat.keepWind.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;

import chat.keepWind.bean.Config;
import chat.keepWind.util.ChatUtil;

/**
 * 
 * @author
 */
public class ChatMainUI{
	/**
	 * 窗体
	 */
	private JFrame winFrame = null;

	/**
	 * 添加、addBtn、setBtn、 noteBabo、jLabel_ui、jPanel_add
	 */
	private JPanel mainjCP = null;// 添加所有组件
	/**
	 * 系统提示喇叭
	 */
	private JLabel noteBabo = null;

	/**
	 * 窗体界面
	 */
	private JLabel jLabel_ui = null;

	/**
	 * 用户头像
	 */
	private JLabel headpic = null;

	/**
	 * 添加网段
	 */
	private JButton addIpBtn = null;

	/**
	 * 查看网段
	 */
	private JButton queryIpBtn = null;

	/**
	 * 添加JTree
	 * 在线用户
	 */
	private JPanel jPanel_add = null;

	/**
	 * 此方法初始化窗体
	 */
	public JFrame getwinFrame() {
		if (winFrame == null) {
			winFrame = new JFrame();
			//winFrame.setSize(new Dimension(227, 480));
			winFrame.setResizable(false);
			winFrame.setTitle("留风IM");			
			winFrame.setContentPane(this.getmainjCP());
			winFrame.setFont(new Font("宋体", Font.PLAIN, 12));
			winFrame.setIconImage(Toolkit.getDefaultToolkit().getImage(
					getClass().getResource("/picture/wind.png")));
			Toolkit toolkit = winFrame.getToolkit();
			Dimension screen = toolkit.getScreenSize();
			winFrame.setBounds(screen.width - 300, 100, 282, 650);
			winFrame.setVisible(true);
			
			
			winFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
		return winFrame;
	}

	/**
	 * 设置好友列表
	 */
	public void setFriend(JTree tree){
		if (jPanel_add == null) {
			jPanel_add = new JPanel();
		}
		jPanel_add.setLayout(new GridLayout(1, 1, 0, 0));
		jPanel_add.setBounds(new Rectangle(1, 66, 226, 450));
		JScrollPane js = new JScrollPane();
		js.getViewport().add(tree);
		jPanel_add.add(js);
	}

	/**
	 * 此方法初始化添加组件后的mainjCP
	 */
	public JPanel getmainjCP() {
		if (mainjCP == null) {
			mainjCP = new JPanel();
			jLabel_ui = new JLabel(new ImageIcon(Toolkit.getDefaultToolkit()
					.getImage(getClass().getResource("/picture/dfthead.png"))));
			jLabel_ui.setBounds(new Rectangle(8, 8, 40, 40));
			
			mainjCP.add(getheadpic());
			noteBabo = new JLabel(new ImageIcon(Toolkit.getDefaultToolkit()
					.getImage(getClass().getResource("/picture/msg.jpg"))));
			mainjCP.add(noteBabo);
			noteBabo.addMouseListener(new MouseAdapter() {
				@Override
				public void mousePressed(MouseEvent e) {
					//Client.client.acceptOrder();
				}
			});
			
			noteBabo.setBounds(new Rectangle(103, 528, 25, 30));
			noteBabo.setFont(new Font("黑体", Font.PLAIN, 15));
			mainjCP.setBackground(Color.white);

			mainjCP.setLayout(null);
			mainjCP.add(getaddBtn());
			mainjCP.add(getaddJPanel());
			mainjCP.add(noteBabo);
			mainjCP.add(jLabel_ui);
			mainjCP.add(getsetBtn());
		}
		return mainjCP;
	}

	
	private JButton getaddBtn() {
		if (addIpBtn == null) {
			addIpBtn = new JButton();
			addIpBtn.setFocusPainted(false);
			addIpBtn.setBounds(new Rectangle(10, 530, 80, 26));
			addIpBtn.setFont(new Font("黑体", Font.BOLD, 12));
			addIpBtn.setText("添加网段");
			addIpBtn.setBackground(null);
			addIpBtn.setBorder(null);
		}
		return addIpBtn;
	}

	/**
	 * 此方法初始化添加好友列表jPanel_add
	 */
	private JPanel getaddJPanel() {
		if (jPanel_add == null) {
			jPanel_add = new JPanel();
			jPanel_add.setLayout(new BoxLayout(jPanel_add, BoxLayout.X_AXIS));
			jPanel_add.setBounds(new Rectangle(0, 78, 226, 255));
			jPanel_add.setBackground(null);
			jPanel_add.repaint();
		}
		return jPanel_add;
	}

	
	private JButton getsetBtn() {
		if (queryIpBtn == null) {
			queryIpBtn = new JButton();
			queryIpBtn.setFocusPainted(false);
			queryIpBtn.setBounds(new Rectangle(136, 530, 80, 26));
			queryIpBtn.setFont(new Font("黑体", Font.BOLD, 12));
			queryIpBtn.setText("查看网段");
			queryIpBtn.setBackground(null);
		}
		return queryIpBtn;
	}

	/**
	 * 返回喇叭 noteBabo
	 */
	public JLabel getnoteBabo() {
		return noteBabo;
	}

	/**
	 * 返回头像headpic
	 */
	public JLabel getheadpic() {
		if (headpic == null) {
			headpic = new JLabel();
			headpic.setBounds(new Rectangle(52, 10, 200, 20));
			headpic.setForeground(Color.BLUE);
			headpic.setFont(new Font("黑体", Font.PLAIN, 14));
			
			Config config = (Config)ChatUtil.dataMap.get("config");
			headpic.setText(config.getLocalNickName());
		}
		return headpic;
	}

	/**
	 * 设置喇叭noteBabo
	 */
	public void setnoteBabo(JLabel label) {
		noteBabo = label;
	}

	
	public JLabel getjLabel_ui() {
		return jLabel_ui;
	}

	public void setjLabel_ui(JLabel jLabel_ui) {
		this.jLabel_ui = jLabel_ui;
	}

	public JButton getAddIpBtn() {
		return addIpBtn;
	}

	public void setAddIpBtn(JButton addIpBtn) {
		this.addIpBtn = addIpBtn;
	}

	public JButton getQueryIpBtn() {
		return queryIpBtn;
	}

	public void setQueryIpBtn(JButton queryIpBtn) {
		this.queryIpBtn = queryIpBtn;
	}
	
}
