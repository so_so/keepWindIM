package chat.keepWind.ui;

import javax.swing.table.DefaultTableModel;

@SuppressWarnings("serial")
public class CustomTableModel extends DefaultTableModel {
	
	public CustomTableModel(String[] columnTitle, Object[][] tableData) {
		super(tableData,columnTitle);
	}
	//重写getColumnClass方法,根据每列的第一个值返回该列真实的数据类型
	@Override
	public Class getColumnClass(int columnIndex) {
		
		return getValueAt(0, columnIndex).getClass();
	}
	@Override
	public boolean isCellEditable(int row, int column) {
		if(column==0){
			return true;
		}
		return false;
	}
	
}
