package chat.keepWind.ui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;

@SuppressWarnings("serial")
public class IpTableUI extends JFrame {
	private JPanel jp1;
	private JPanel jp2;
	private JTable table;
	private JButton jbut;
	private  CustomTableModel model;
	
	public IpTableUI(Object[][] tableData){
		
		jp1 = new JPanel();
		jp2 = new JPanel();
		
		String[] columnTitle = {" ","局域网段"};
		model = new CustomTableModel(columnTitle, tableData);
		table = new JTable(model);
		table.setSize(380, 300);
		table.setRowHeight(20);	
		DefaultTableCellRenderer r = new DefaultTableCellRenderer();
		r.setHorizontalAlignment(JLabel.CENTER);
		table.setDefaultRenderer(Object.class, r);
		table.getTableHeader().setReorderingAllowed(false);
		
		//设置第一列 列宽
		TableColumnModel cols = table.getColumnModel();
		//cols.getColumn(0).setPreferredWidth(100);
		cols.getColumn(0).setMinWidth(150);
		cols.getColumn(0).setMaxWidth(150);
		table.setColumnModel(cols);
		
		jbut = new JButton("  删除  ");
		jbut.setFont(new Font("黑体", Font.PLAIN, 12));
		jbut.setForeground(SystemColor.menuText);
		
		JScrollPane jScrollPane = new JScrollPane(table);
		jp1.add(jScrollPane,BorderLayout.CENTER);
		jp2.add(jbut);
		
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(
				getClass().getResource("/picture/ip.png")));
		this.setResizable(false);
		this.setBounds(400, 200, 400, 400);
		this.add(jp1,BorderLayout.CENTER);
		this.add(jp2,BorderLayout.SOUTH);
		this.setVisible(true);
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JButton getJbut() {
		return jbut;
	}

	public void setJbut(JButton jbut) {
		this.jbut = jbut;
	}

	public CustomTableModel getModel() {
		return model;
	}

	public void setModel(CustomTableModel model) {
		this.model = model;
	}
	
}
