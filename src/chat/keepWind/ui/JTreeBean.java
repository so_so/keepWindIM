/**
 * 好友列表节点
 */
package chat.keepWind.ui;

import java.awt.Color;
import java.awt.Font;

import javax.swing.ImageIcon;

/**
 * 
 * @author
 */
public class JTreeBean {

	private String nick;
	private ImageIcon iconNode;
	private Color color;
	private String ip;
	private Font font = new Font("宋体", Font.PLAIN, 14);//默认字体

	public JTreeBean(String nick, ImageIcon icon, Color color,String ip) {
		this.nick = nick;
		this.iconNode = icon;
		this.color = color;
		this.ip = ip;
	}

	public JTreeBean(String nick, ImageIcon icon) {
		this.nick = nick;
		this.iconNode = icon;
	}

	public JTreeBean(String nick) {
		this.nick = nick;
	}
	
	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	
	public ImageIcon getIcon() {
		return iconNode;
	}

	public void setIcon(ImageIcon iconNode) {
		this.iconNode = iconNode;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
	
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Font getFont() {
		return font;
	}

	public void setFont(Font font) {
		this.font = font;
	}
	
}
