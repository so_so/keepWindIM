package chat.keepWind.handle;

import java.util.List;

import chat.keepWind.bean.DataPackage;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.socket.DatagramPacket;
import io.netty.handler.codec.MessageToMessageDecoder;
import io.netty.util.CharsetUtil;

public class KeepWindDecoder extends MessageToMessageDecoder<DatagramPacket> {
	@Override
    protected void decode(ChannelHandlerContext ctx, DatagramPacket datagramPacket, List<Object> out) throws Exception {
       ByteBuf data = datagramPacket.content(); 
       
       
       DataPackage dataPkg = new DataPackage();
       dataPkg.setFlag(data.readByte());
       byte[] tmp = new byte[data.readByte()];
       data.readBytes(tmp);
       dataPkg.setMsgType(new String(tmp,CharsetUtil.UTF_8));
       tmp = new byte[data.readByte()];
       data.readBytes(tmp);
       dataPkg.setFromIp(new String(tmp,CharsetUtil.UTF_8));
       dataPkg.setPort(data.readInt());
       
       tmp = new byte[data.readByte()];
       data.readBytes(tmp);
       dataPkg.setToIp(new String(tmp,CharsetUtil.UTF_8));
       tmp = new byte[data.readByte()];
       data.readBytes(tmp);
       dataPkg.setNickName(new String(tmp,CharsetUtil.UTF_8));
       tmp = new byte[data.readByte()];
       data.readBytes(tmp);
       dataPkg.setSendTime(new String(tmp,CharsetUtil.UTF_8));
       
      
       tmp = new byte[data.readInt()];
       
       data.readBytes(tmp);
       dataPkg.setMsgContent(tmp);
       
     
       tmp = new byte[data.readShort()];
       data.readBytes(tmp);
       dataPkg.setFileName(new String(tmp,CharsetUtil.UTF_8));
       tmp = new byte[data.readShort()];
       data.readBytes(tmp);
       dataPkg.setSrcPath(new String(tmp,CharsetUtil.UTF_8));
       tmp = new byte[data.readShort()];
       data.readBytes(tmp);
       dataPkg.setDistPath(new String(tmp,CharsetUtil.UTF_8));
      
      
       dataPkg.setPoint(data.readLong());
       dataPkg.setFileLen(data.readLong());
       dataPkg.setFileNo(data.readInt());
       //System.out.println("解码："+dataPkg.toString());
       out.add(dataPkg);
       
       
    }
}
