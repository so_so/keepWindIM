package chat.keepWind.handle;

import java.net.InetSocketAddress;
import java.util.List;

import chat.keepWind.bean.DataPackage;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.socket.DatagramPacket;
import io.netty.handler.codec.MessageToMessageEncoder;
import io.netty.util.CharsetUtil;

public class KeepWindEncoder extends MessageToMessageEncoder<DataPackage> {
	  private  InetSocketAddress remoteAddress;

	    public KeepWindEncoder(InetSocketAddress remoteAddress) { 
	        this.remoteAddress = remoteAddress;
	    }

	    @Override
	    protected void encode(ChannelHandlerContext ctx, 
	    		DataPackage data, List<Object> out) throws Exception {
	    	
	    	
	    	//System.out.println("编码："+data.toString());
	    	
	    	//服务反馈到发送过来的地址
	    	if(data.getFlag()==1){
	    		//主动发送
	    	}else{
	    		//接受方 ，反馈
	    		remoteAddress = new InetSocketAddress(data.getToIp(),data.getPort());
	    	}
	    	
	    	//int len=0;
	    	byte[] fileName = data.getFileName().getBytes(CharsetUtil.UTF_8);
	        byte[] msg = data.getMsgContent(); 
	        byte[] fromIp = data.getFromIp().getBytes(CharsetUtil.UTF_8);
	        byte[] toIp = data.getToIp().getBytes(CharsetUtil.UTF_8);
	        byte[] nickName = data.getNickName().getBytes(CharsetUtil.UTF_8);
	        byte[] msgType = data.getMsgType().getBytes(CharsetUtil.UTF_8);
	        byte[] sendTime = data.getSendTime().getBytes(CharsetUtil.UTF_8);
	        byte[] srcPath = data.getSrcPath().getBytes(CharsetUtil.UTF_8);
	        byte[] distPath = data.getDistPath().getBytes(CharsetUtil.UTF_8);
	        long point = data.getPoint();
	        long fileLen = data.getFileLen();
	        
	        /*len = fileName.length+msg.length+fromIp.length+msgType.length
	        		+toIp.length+sendTime.length+srcPath.length+distPath.length+4+
	        		+2*3+4+8*2;*/
	        
	        //ByteBuf buf = ctx.alloc().buffer(len);
	        ByteBuf buf = ctx.alloc().directBuffer();
	        buf.writeByte(data.getFlag());
	        buf.writeByte((byte)msgType.length);
	        buf.writeBytes(msgType);
	        buf.writeByte((byte)fromIp.length);
	        buf.writeBytes(fromIp);
	        buf.writeInt(data.getPort());
	        
	        buf.writeByte((byte)toIp.length);
	        buf.writeBytes(toIp);
	        buf.writeByte((byte)nickName.length);
	        buf.writeBytes(nickName);
	        buf.writeByte((byte)sendTime.length);
	        buf.writeBytes(sendTime); 
	        
	        buf.writeInt(msg.length);
	        buf.writeBytes(msg);  
	        
	        buf.writeShort((short)fileName.length); 
	        buf.writeBytes(fileName); 
	        buf.writeShort((short)srcPath.length);
	        buf.writeBytes(srcPath); 
	        buf.writeShort((short)distPath.length);
	        buf.writeBytes(distPath); 
	       
	        buf.writeLong(point);
	        buf.writeLong(fileLen);
	        buf.writeInt(data.getFileNo());
	        out.add(new DatagramPacket(buf, remoteAddress)); 
	    }
}
