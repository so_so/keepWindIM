package chat.keepWind.event;

import java.net.InetSocketAddress;

import chat.keepWind.bean.DataPackage;
import chat.keepWind.handle.KeepWindDecoder;
import chat.keepWind.handle.KeepWindEncoder;
import chat.keepWind.handle.OnlineInboundHandler;
import chat.keepWind.util.GlobalConst;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioDatagramChannel;

public class SendOnline {
	
	public void send(final String remoteIp,DataPackage dataPkg) {
        EventLoopGroup eventLoopGroup = new NioEventLoopGroup(1);
        Bootstrap bootstrap = new Bootstrap();
        try {
            bootstrap.group(eventLoopGroup).channel(NioDatagramChannel.class)
                    .option(ChannelOption.SO_BROADCAST, true)
                    .handler(new ChannelInitializer<Channel>() {
                        @Override
                        protected void initChannel(Channel channel) throws Exception {
                            ChannelPipeline pipeline = channel.pipeline();
                            pipeline.addLast(new KeepWindDecoder()); //解密
                            pipeline.addLast(new KeepWindEncoder(new InetSocketAddress(remoteIp,GlobalConst.RCV_PORT))); //编码
                            pipeline.addLast(new OnlineInboundHandler());
                        }
                    });
            //绑定随机端口，防止固定端口有可能被占用
            Channel channel = bootstrap.bind(0).sync().channel();
            
            int port = ((InetSocketAddress)channel.localAddress()).getPort();
            dataPkg.setPort(port);
            
            channel.writeAndFlush(dataPkg).sync();
            
            if (!channel.closeFuture().await(90)) {//等待90毫秒
                //System.out.println("out of time");
            }
           //等待关闭
            //channel.closeFuture().sync();
        } catch (Exception e) {
            //e.printStackTrace();
        	System.out.println("端口bind错误");
        } finally {
            eventLoopGroup.shutdownGracefully();
        }
    }
}
