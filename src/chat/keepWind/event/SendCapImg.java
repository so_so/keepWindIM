package chat.keepWind.event;

import java.io.File;
import java.io.RandomAccessFile;
import java.net.InetSocketAddress;
import java.util.List;

import chat.keepWind.bean.DataPackage;
import chat.keepWind.handle.KeepWindEncoder;
import chat.keepWind.util.GlobalConst;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioDatagramChannel;

public class SendCapImg {
	
	public void send(final String remoteIp,List<DataPackage> imgPkgs) {
        EventLoopGroup eventLoopGroup = new NioEventLoopGroup(1);
        Bootstrap bootstrap = new Bootstrap();
        try {
            bootstrap.group(eventLoopGroup).channel(NioDatagramChannel.class)
                    .option(ChannelOption.SO_BROADCAST, true)
                    .handler(new ChannelInitializer<Channel>() {
                        @Override
                        protected void initChannel(Channel channel) throws Exception {
                            ChannelPipeline pipeline = channel.pipeline();
                            //pipeline.addLast(new KeepWindDecoder()); //解密
                            pipeline.addLast(new KeepWindEncoder(new InetSocketAddress(remoteIp,GlobalConst.RCV_PORT))); //编码
                           //pipeline.addLast(new KeepWindInboundHandler());//处理数据
                        }
                    });
            Channel channel = bootstrap.bind(GlobalConst.SND_PORT).sync().channel();
            
            for(DataPackage imgPkg : imgPkgs){
            	File file = new File(imgPkg.getSrcPath());
                long len = file.length();
                long pointer = 0;
                while(true) {
                     
                    if (len <= pointer+1) {//文件指针已到末尾
                        //开始发送文本消息
                        break;
                    } else if (len > pointer) {
                        
                        RandomAccessFile raf = new RandomAccessFile(file, "r");
                      
                        raf.seek(pointer); 
                        
                        byte[] bytes = null;
                        if(len-pointer>=GlobalConst.BYTE_NUM){
                        	bytes = new byte[GlobalConst.BYTE_NUM];
                        }else{
                        	bytes = new byte[(int)(len-pointer)];
                        }
                        raf.read(bytes);//读取
                        imgPkg.setMsgContent(bytes);
                        
                        channel.writeAndFlush(imgPkg).sync();
                        
                        pointer = raf.getFilePointer();
                
                        imgPkg.setPoint(pointer);//设置文件指针位置
                        
                        raf.close();
                    }
                   
                }
            }
            channel.closeFuture();
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            eventLoopGroup.shutdownGracefully();
        }
    }
}
